using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PangController : PangElement
{
    bool isMoving = false;
    int direction = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            app.view.PlayerMovement(direction);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            FireBullet();
        }
    }

    public void MoveLeft()
    {
        Debug.Log("MoveLeft");
        isMoving = true;
        direction = -1;
    }
    public void MoveRight()
    {
        Debug.Log("MoveRight");
        isMoving = true;
        direction = 1;
    }
    public void MoveStop()
    {
        Debug.Log("MoveStop");
        isMoving = false;
    }

    public void FireBullet()
    {
        app.view.CreateBullet();
    }
}
