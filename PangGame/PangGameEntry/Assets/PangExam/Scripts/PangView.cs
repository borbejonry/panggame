using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Contains all views related to the app.
public class PangView : PangElement
{
    //References to camera walls
    [SerializeField] GameObject wallTop;
    [SerializeField] GameObject wallBottom;
    [SerializeField] GameObject wallRight;
    [SerializeField] GameObject wallLeft;

    // References to all balls
    public List<BallBounce> balls = new List<BallBounce>();
    [SerializeField] GameObject ballToClone;
    [SerializeField] Transform ballParent;

    [Space]
    [SerializeField] Transform player;

    [Space]
    [SerializeField] GameObject bulletToClone;

    [Space]
    [SerializeField] GameObject userInterface;
    [SerializeField] GameObject hpToClone;
    [SerializeField] Transform hpParent;
    [SerializeField] List<GameObject> healths;

    [Space]
    [SerializeField] Button winBtn;
    [SerializeField] Button loseBtn;
    [SerializeField] Button backBtn;

    [Space]
    [SerializeField] GameObject winScreen;
    [SerializeField] GameObject loseScreen;

    Camera m_camera;
    Rect m_screenSafeArea;

    private void OnDestroy()
    {
        winBtn.onClick.RemoveListener(Winner);
        loseBtn.onClick.RemoveListener(Losed);
    }
    private void Start()
    {
        winBtn.onClick.AddListener(Winner);
        loseBtn.onClick.AddListener(Losed);
        backBtn.onClick.AddListener(Winner);
    }

    private void Losed()
    {
        SceneManager.LoadScene(0);
    }

    private void Winner()
    {
        SceneManager.LoadScene(0);
    }

    public void InitializeWall()
    {
        m_camera = Camera.main;


        m_screenSafeArea = Screen.safeArea;

        Vector2 _size = m_camera.ScreenToWorldPoint(new Vector3(m_screenSafeArea.xMin, m_screenSafeArea.yMax, 0f));
        _size.x = Mathf.Abs(_size.x) * 2;
        _size.y = Mathf.Abs(_size.y) * 2;

        //wallTop.transform.localScale = new Vector3(m_screenSafeArea.xMin, 1f, 1f);
        wallTop.transform.localScale = new Vector3(_size.x, 1f, 1f);
        wallTop.transform.position = new Vector3(0f, m_camera.ScreenToWorldPoint(new Vector3(0f, m_screenSafeArea.yMax, 0f)).y, 0f);

        //wallBottom.transform.localScale = new Vector3(m_screenSafeArea.xMin, 1f, 1f);
        wallBottom.transform.localScale = new Vector3(_size.x, 1f, 1f);
        wallBottom.transform.position = new Vector3(0, m_camera.ScreenToWorldPoint(new Vector3(0f, m_screenSafeArea.yMin, 0f)).y, 0f);

        //wallLeft.transform.localScale = new Vector3(1f, m_screenSafeArea.yMin, 1f);
        wallLeft.transform.localScale = new Vector3(1f, _size.y, 1f);
        wallLeft.transform.position = new Vector3(m_camera.ScreenToWorldPoint(new Vector3(m_screenSafeArea.xMin, 0f, 0f)).x, 0f, 0f);

        //wallRight.transform.localScale = new Vector3(1f, m_screenSafeArea.yMin, 1f);
        wallRight.transform.localScale = new Vector3(1f, _size.y, 1f);
        wallRight.transform.position = new Vector3(m_camera.ScreenToWorldPoint(new Vector3(m_screenSafeArea.xMax, 0f, 0f)).x, 0f, 0f);

    }

    public void CreateBalls(PangModel.BALL_TYPE _type, Transform _parent = null)
    {
        if (_parent == null)
        {
            _parent = ballParent;
        }

        BallBounce _ball1 = Instantiate(ballToClone, _parent).GetComponent<BallBounce>();
        BallBounce _ball2 = Instantiate(ballToClone, _parent).GetComponent<BallBounce>();

        balls.Add(_ball1);
        balls.Add(_ball2);

        _ball1.transform.SetParent(null);
        _ball2.transform.SetParent(null);

        float scale = 10;
        float forceY = 3;
        switch (_type)
        {
            case PangModel.BALL_TYPE.LARGE:
                scale = 10;
                forceY = 8;
                break;
            case PangModel.BALL_TYPE.MEDIUM:
                scale = 5;
                forceY = 5;
                break;
            case PangModel.BALL_TYPE.SMALL:
                scale = 3;
                forceY = 3;
                break;
            default: break;
        }
        _ball1.gameObject.transform.localScale = new Vector3(scale, scale, scale);
        _ball2.gameObject.transform.localScale = new Vector3(scale, scale, scale);

        _ball1.BallCreated(3, forceY, _type);
        _ball2.BallCreated(-3, forceY, _type);
    }


    public void PlayerMoveLeft()
    {
        app.controller.MoveLeft();
    }
    public void PlayerMoveRight()
    {
        app.controller.MoveRight();
    }
    public void PlayerMoveRelease()
    {
        app.controller.MoveStop();
    }

    public void PlayerMovement(int dir)
    {
        if (dir > 0)
        {
            //right
            player.transform.Translate(Vector3.right * app.model.moveSpeedPlayer * Time.deltaTime);
        }
        else
        {
            //left
            player.transform.Translate(Vector3.left * app.model.moveSpeedPlayer * Time.deltaTime);
        }
    }

    public void CreateBullet()
    {
        GameObject _obj = Instantiate(bulletToClone, player);
        _obj.transform.SetParent(null);
    }
    public void CreateHP()
    {
        for (int i = 0; i < app.model.maxLife; i++)
        {
            GameObject _obj = Instantiate(hpToClone, hpParent);
            _obj.SetActive(true);
            healths.Add(_obj);
        }
    }

    public void ReduceHP()
    {
        if (healths.Count != 0)
        {
            int _index = healths.Count - 1;
            GameObject _obj = healths[_index];
            healths.RemoveAt(_index);
            Destroy(_obj);
        }

        app.model.ReduceHP();

        //set win/lose condition
        if (app.model.curLife == 0)
        {
            Time.timeScale = 0;
            foreach (BallBounce data in balls)
            {
                data.DestroyThis();
            }
            balls.Clear();
            Time.timeScale = 1;

            ShowUserInterface(false);
            loseScreen.SetActive(true);
        }
    }

    public void ShowUserInterface(bool isOn)
    {
        userInterface.SetActive(isOn);
    }

    public void CheckWinCondition()
    {
        if (loseScreen.activeSelf)
        {
            winScreen.SetActive(false);
        }
        else
        {
            if (balls.Count == 0)
            {
                winScreen.SetActive(true);
            }
        }
    }
}
