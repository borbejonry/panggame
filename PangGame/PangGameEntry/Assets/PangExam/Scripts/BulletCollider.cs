﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollider : MonoBehaviour
{
    [SerializeField] Bullet m_bullet;

    private void OnCollisionEnter2D(Collision2D _collision)
    {
        if (_collision.gameObject.layer == (int)PangModel.ADDED_LAYER.TOPWALL)
        {
            DestroyBullet();
        }
    }

    public void DestroyBullet()
    {
        Destroy(m_bullet.gameObject);
    }
}
