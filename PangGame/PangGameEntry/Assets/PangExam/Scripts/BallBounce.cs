using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This is a view
public class BallBounce : PangElement
{
    public Vector3 force;
    Rigidbody2D m_rb;
    PangModel.BALL_TYPE m_ballType;
    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
    }

    private void Update()
    {

    }

    // Callback called upon collision.
    private void OnCollisionEnter2D(Collision2D _collision)
    {
        if (_collision.gameObject.layer == (int)PangModel.ADDED_LAYER.BULLETS)
        {
            switch (m_ballType)
            {
                case PangModel.BALL_TYPE.SMALL:
                    HitBall(PangModel.BALL_TYPE.NONE);
                    break;
                case PangModel.BALL_TYPE.MEDIUM:
                    HitBall(PangModel.BALL_TYPE.SMALL);
                    break;
                case PangModel.BALL_TYPE.LARGE:
                    HitBall(PangModel.BALL_TYPE.MEDIUM);
                    break;
                default: break;
            }

            _collision.gameObject.GetComponent<BulletCollider>().DestroyBullet();
        }

        if (_collision.gameObject.tag == "Player")
        {
            switch (m_ballType)
            {
                case PangModel.BALL_TYPE.SMALL:
                    HitBall(PangModel.BALL_TYPE.NONE);
                    break;
                case PangModel.BALL_TYPE.MEDIUM:
                    HitBall(PangModel.BALL_TYPE.SMALL);
                    break;
                case PangModel.BALL_TYPE.LARGE:
                    HitBall(PangModel.BALL_TYPE.MEDIUM);
                    break;
                default: break;
            }

            app.view.ReduceHP();
        }
    }

    public void BallCreated(float _forceX, float _forceY, PangModel.BALL_TYPE _type)
    {
        force = new Vector3(_forceX, _forceY, 0);
        m_rb.AddForce(force, ForceMode2D.Impulse);

        m_ballType = _type;
    }

    void HitBall(PangModel.BALL_TYPE _type)
    {
        if (_type == PangModel.BALL_TYPE.NONE)
        {

        }
        else
        {
            app.view.CreateBalls(_type, this.transform);
        }

        app.view.balls.Remove(this);
        DestroyThis();

        app.view.CheckWinCondition();
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
