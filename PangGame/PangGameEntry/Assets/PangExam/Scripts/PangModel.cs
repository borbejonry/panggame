using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contains all data related to the app.
public class PangModel : PangElement
{
    public enum BALL_TYPE
    {
        NONE,
        SMALL,
        MEDIUM,
        LARGE
    }
    public enum ADDED_LAYER
    {
        BALLS = 8,
        WALLS = 9,
        PLAYERS = 10,
        BULLETS = 11,
        TOPWALL = 12
    }

    // Data
    public int winCondition;
    public int curLife = 5;
    public int maxLife = 5;
    public float moveSpeedPlayer = 3;

    public void RestartGame()
    {
        curLife = maxLife;
    }

    public void ReduceHP()
    {
        curLife--;

        if (curLife < 0)
        {
            curLife = 0;
        }
    }
}
