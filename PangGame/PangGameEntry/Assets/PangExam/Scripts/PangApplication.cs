using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PangApplication : MonoBehaviour
{
    // Reference to the root instances of the MVC.
    public PangModel model;
    public PangView view;
    public PangController controller;

    Vector2 prevScreen;
    Vector2 curScreen;

    // Start here and Initialize things here
    void Start() 
    {
        curScreen = new Vector2(Screen.safeArea.width, Screen.safeArea.height);
        view.InitializeWall();

        RestartGame();
    }

#if UNITY_WEBGL
    private void Update()
    {
        if (prevScreen != curScreen)
        {
            curScreen = new Vector2(Screen.safeArea.width, Screen.safeArea.height);
            prevScreen = new Vector2(Screen.safeArea.width, Screen.safeArea.height);
            view.InitializeWall();
        }
    }
#endif

    public void RestartGame()
    {
        view.ShowUserInterface(true);
        view.CreateBalls(PangModel.BALL_TYPE.LARGE);
        model.RestartGame();
        view.CreateHP();
    }
}
