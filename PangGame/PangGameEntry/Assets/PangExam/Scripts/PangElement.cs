using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PangElement : MonoBehaviour
{
    // Gives access to the application and all instances.
    public PangApplication app
    { 
        get 
        { 
            return GameObject.FindObjectOfType<PangApplication>(); 
        } 
    }
}
